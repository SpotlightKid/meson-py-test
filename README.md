# meson-py-test

A simple test project for building and installing a Python package with meson

```console
$ meson setup build --prefix=/usr
$ sudo meson install -C build
```

Assuming your Python version is 3.9, this will install the Python package `mypymod`
to `<prefix>/lib/python3.9/site-packages/mypymod`, where `<prefix>` corresponds
to the path given to to the `--prefix` option to `meson setup`.

Test:

```console
$ python3 -m mypymod
Hello, from mypymod!
```
